package spec

import geb.spock.GebReportingSpec
import page.BlogPage
import page.StartPage

class NixNavigationSpec extends GebReportingSpec{
    def "Navigate to main page"() {
        when:
        to StartPage
        
        and :
        "User navigates to Blog page"()

        then:
        at BlogPage

    }
}